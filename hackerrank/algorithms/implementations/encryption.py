#!/bin/python

import math

s = raw_input().strip()
low = int(math.floor(math.sqrt(len(s))))
high = int(math.ceil(math.sqrt(len(s))))
a = []
for i in xrange(high):
    a += [s[i::high]]
print ' '.join(a) 