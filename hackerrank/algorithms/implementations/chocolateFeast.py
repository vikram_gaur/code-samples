# Enter your code here. Read input from STDIN. Print output to STDOUT
def calc(n, re):
    result = 0
    while n>=re:
        temp = n%re
        n /= re
        result += n
        n+=temp
    return result

T = int(raw_input())
for i in range (0,T):
    A,B,C = [int(x) for x in raw_input().split(' ')]
    initial = A/B
    answer = initial + calc(initial, C)
    # write code to compute answer
    print answer
