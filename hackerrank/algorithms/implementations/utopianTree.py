T = input()
for _ in xrange(T):
    N = input()
    x = 2**((N/2)+1) - 1
    if N%2==0:
        print x
    else:
        print 2*x