# Enter your code here. Read input from STDIN. Print output to STDOUT
def kadane(ar):
    current = 0
    best = 0
    for i in ar:
        current += i
        if current < 0:
            current = 0
        if current > best:
            best = current
    answer = best
    if(best == 0):
        answer = max(ar)
    return answer
        
t = input()
for i in xrange(t):
    n = input()
    ar = map(int, str(raw_input()).split())
    array = [x for x in ar if x > 0]
    if(len(array) == 0):
        answer = max(ar)
    else:
        answer = sum(array)
    print kadane(ar), answer