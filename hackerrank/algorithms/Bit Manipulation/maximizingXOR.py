#!/bin/python

def  maxXor( l,  r):
    first = bin(l)
    first = first[2:]
    last = bin(r)
    last = last[2:]
    common_len = max(len(first), len(last))
    first = first.zfill(common_len)
    first = list(first)
    last = last.zfill(common_len)
    last = list(last)
    for i in xrange(common_len):
        if(first[i] != last[i]):
            break
        first[i] = '0'
    while(i<common_len):
        first[i] = '1'
        i+=1
    return int(''.join(first), 2)

_l = int(raw_input());
_r = int(raw_input());

res = maxXor(_l, _r);
print(res)

