# Enter your code here. Read input from STDIN. Print output to STDOUT
def bfs(g, s):
    distance = [-1]*len(g)
    q = [s]
    distance[s] = 0
    while(q!=[]):
        a = q.pop()
        for x in g[a]:
            if(distance[x] == -1):
                q.insert(0,x)
                distance[x] = distance[a]+6
    distance.remove(0)
    return distance

t = input()
for _ in xrange(t):
    n,m = map(int, str(raw_input()).split())
    mapping = []
    for i in xrange(n):
        mapping.insert(0,[])
    for i in xrange(m):
        x,y = map(int, str(raw_input()).split())
        mapping[x-1] += [y-1]
        mapping[y-1] += [x-1]
    s = input()
    a = bfs(mapping, s-1)
    print ' '.join(map(str,a))
