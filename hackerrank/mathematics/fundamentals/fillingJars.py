result = 0
jars, op = map(int, str(raw_input()).split())
for _ in xrange(op):
    a, b, marbles = map(int, str(raw_input()).split())
    result += (b-a+1)*marbles
result /= jars
print result