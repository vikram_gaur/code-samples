# Enter your code here. Read input from STDIN. Print output to STDOUT
from collections import Counter

a = Counter(str(raw_input()))
x = a.most_common(3)
if(x[0][1] > x[1][1]):
    print x[0][0], x[0][1]
    if((x[1][1] > x[2][1]) or (x[1][0] < x[2][0])):
        print x[1][0], x[1][1]
        print x[2][0], x[2][1]
    else:
        print x[2][0], x[2][1]
        print x[1][0], x[1][1]
else:
    if(x[1][1] > x[2][1]):
        if(x[0][0] > x[1][0]):
            print x[1][0], x[1][1]
            print x[0][0], x[0][1]
        else:
            print x[0][0], x[0][1]
            print x[1][0], x[1][1]
        print x[2][0], x[2][1]
    else:
        for i,j in sorted(x, key=lambda x:x[0]):
            print i,j