# Enter your code here. Read input from STDIN. Print output to STDOUT
def compare(x,y):
    if(x.islower() or y.islower()):
        if(x.islower() and y.islower()): return ord(x)-ord(y)
        return 1 if y.islower() else -1
    if(x.isupper() or y.isupper()):
        if(x.isupper() and y.isupper()): return ord(x)-ord(y)
        return 1 if y.isupper() else -1
    if(int(x)%2 != 0 or int(y)%2 != 0):
        if(int(x)%2!=0 and int(y)%2!=0): return int(x)-int(y)
        return 1 if int(y)%2!= 0 else -1
    return int(x)-int(y)
print reduce(lambda x,y:x+y, sorted(raw_input().strip(), cmp=compare))