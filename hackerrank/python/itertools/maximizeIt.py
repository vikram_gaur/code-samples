# Enter your code here. Read input from STDIN. Print output to STDOUT
from itertools import product

k,m = map(int, str(raw_input()).split())
a = []
for i in xrange(k):
    x = map(lambda x: int(x)%m,str(raw_input()).split())
    a += [x[1:]]
answer = 0
for i in product(*a):
    s = 0
    for j in i:
        s += j*j % m
    s %= m
    if(answer < s):
        answer = max(s,answer)
print answer